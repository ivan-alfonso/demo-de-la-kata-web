__author__ = 'ivan'
from unittest import TestCase
from selenium import webdriver
import time

class FunctionalTest(TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(executable_path="/Users/IvanAlfonso/Documents/Gecko Driver/chromedriver")
        self.browser.implicitly_wait(2)

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

    def test_registro(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_register')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.send_keys('Ivan David')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Alfonso')

        experiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        experiencia.send_keys('2')

        self.browser.find_element_by_xpath("//select[@id='id_tiposDeServicio']/option[text()='Desarrollador Web']").click()
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3105555555')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('ivan.alfonso@email.com')

        imagen = self.browser.find_element_by_id('id_imagen')
        imagen.send_keys('/Users/IvanAlfonso/Downloads/python.png')

        nombreUsuario = self.browser.find_element_by_id('id_username')
        nombreUsuario.send_keys('ivan123')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)
        span=self.browser.find_element(By.XPATH, '//span[text()="Ivan David Alfonso"]')

        self.assertIn('Ivan David Alfonso', span.text)



    def test_verDetalle(self):
        self.browser.get('http://localhost:8000')
        span = self.browser.find_element(By.XPATH, '//span[text()="Ivan David Alfonso"]')
        span.click()

        h2 = self.browser.find_element(By.XPATH, '//h2[text()="Ivan David Alfonso"]')

        self.assertIn('Ivan David Alfonso', h2.text)

    def test_login(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_login')
        link.click()

        nombreUsuario = self.browser.find_element_by_id('idusername')
        nombreUsuario.send_keys('ivan123')

        clave = self.browser.find_element_by_id('idpassword')
        clave.send_keys('clave123')

        botonLogin = self.browser.find_element_by_id('idlogin')
        botonLogin.click()
        self.browser.implicitly_wait(3)
        link2 = self.browser.find_element_by_id("id_logout")

        self.assertIn('Logout', link2.text)
        time.sleep(5)


    def test_editUser(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_login')
        link.click()

        nombreUsuario = self.browser.find_element_by_id('idusername')
        nombreUsuario.send_keys('ivan123')

        clave = self.browser.find_element_by_id('idpassword')
        clave.send_keys('clave123')

        botonLogin = self.browser.find_element_by_id('idlogin')
        botonLogin.click()
        time.sleep(3)


        link = self.browser.find_element_by_id('id_editar')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.clear()
        nombre.send_keys('Cristian')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.clear()
        apellidos.send_keys('Olaya')

        experiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        experiencia.clear()
        experiencia.send_keys('5')

        botonEdit = self.browser.find_element_by_id('id_grabar')
        botonEdit.click()
        self.browser.implicitly_wait(3)
        link3 = self.browser.find_element_by_id("name")

        self.assertIn('Cristian Olaya', link3.text)


    def test_comentario(self):
        self.browser.get('http://localhost:8000')
        span = self.browser.find_element(By.XPATH, '//span[text()="Cristian Olaya"]')
        span.click()

        correo = self.browser.find_element_by_id('correo')
        correo.send_keys('colaya@uniandes.edu.co')

        nombre = self.browser.find_element_by_id('comentario')
        nombre.send_keys('Este es un nuevo comentario')

        botonEdit = self.browser.find_element_by_id('comentar')
        botonEdit.click()

        h4 = self.browser.find_element(By.XPATH, '//h4[text()="colaya@uniandes.edu.co"]')
        self.assertIn('colaya@uniandes.edu.co', h4.text)